/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pakorn.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Kirito
 */
public class OXProgram {
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void ShowWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void ShowTable() {
        System.out.println(" 123");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println("");
        }
    }

    static void ShowTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
//              System.out.println("row: "+ row + " Col: "+ col);
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Eror: table at row and col is not empty!!!");
        }

    }
    static void CheckCol(){
        for(int row=0; row<3; row++){
            if(table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
     static void CheckRow(){
        for(int col=0; col<3; col++){
            if(table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
     static void CheckX1() {
        for(int i=0; i<3; i++){
            if(table[i][i] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
      static void CheckX2() {
        for(int i=0; i<3; i++){
            if(table[2-1][i] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
     static void CheckDraw() {
         for(int row=0; row< 3 ; row++){
            for(int col=0; col< 3 ; col++){
                if(table[row][col] == '-'){
                    return;
                }
            }
        }
        isFinish = true;
        winner = player;
     }
    static void CheckWin() {
        CheckRow();
        CheckCol();
        CheckX1();
        CheckX2();
        CheckDraw();
    }
    
    static void SwitchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void ShowResult() {
        if(winner=='-'){
            System.out.println("Draw");
        }else {
             System.out.println(winner+" Win!!!");
        }
        
        
    }

    static void ShowBye() {
        System.out.println("Bye Bye");
    }

    public static void main(String[] args) {
        ShowWelcome();
        do {
            ShowTable();
            ShowTurn();
            input();
            CheckWin();
            SwitchPlayer();
        } while (!isFinish);
        ShowResult();
        ShowBye();

    }

}
